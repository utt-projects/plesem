from django.shortcuts import render

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework import generics

from core.models import GrantGoal
from .serializers import ListGrantGoalSerializer, DetailGrantGoalSerializer, CreateGrantGoalSerializer, UpdateGrantGoalSerializer
# Create your views here.


#### API GRANTGOAL ####

## CREATE
class CreateGrantGoalAPIView(generics.CreateAPIView):
    serializer_class = CreateGrantGoalSerializer

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)


## RETRIEVE
# LIST
class ListGrantGoalAPIView(APIView):
    def get(self, request):
        queryset = GrantGoal.objects.all()
        data = ListGrantGoalSerializer(queryset, many=True).data
        return Response(data)


# DETAIL
class DetailGrantGoalAPIView(APIView):
    def get(self, request, pk):
        queryset = GrantGoal.objects.get(pk=pk)
        data = DetailGrantGoalSerializer(queryset, many=False).data
        return Response(data)


## UPDATE
class UpdateGrantGoalAPIView(generics.UpdateAPIView):
    serializer_class = UpdateGrantGoalSerializer
    queryset = GrantGoal.objects.all()
    


## DELETE
class DeleteGrantGoalAPIView(generics.DestroyAPIView):
    serializer_class = DetailGrantGoalSerializer
    queryset = GrantGoal.objects.all()